class CfgPatches
{
	class DZR_IDz_scripts
	{
		requiredAddons[] = {"DZ_Data"};
		units[] = {};
		weapons[] = {};
	};
};
class CfgMods
{
	class DZR_IDz
	{
		type = "mod";
		author = "DayZRussia";
		description = "Recognizing players from distance. Inspired by Wardog's Check Identity.";
		dir = "DZR_IDz";
		name = "DZR IdentityZ";
		inputs = "DZR_IDz/Data/Inputs.xml";
		dependencies[] = {"Game","World","Mission"};
		class defs
		{
			class gameScriptModule
			{
				files[] = {"DZR_IDz/3_Game"};
			};
			class worldScriptModule
			{
				files[] = {"DZR_IDz/4_World"};
			};
			class missionScriptModule
			{
				files[] = {"DZR_IDz/5_Mission"};
			};
		};
	};
};
