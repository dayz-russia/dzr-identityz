modded class MissionGameplay
{
	
	int RED_COLOR = ARGB(255, 255, 0, 0);
	int LIME_COLOR = ARGB(255, 0, 255, 0);
	int DEFAULT = ARGB(100, 255, 255, 255);
	
	protected Widget m_DZRIDz_root;
	protected Widget m_DZRIDz_root2;
	protected TextWidget m_dzrCenterText;
	protected TextWidget m_dzr_EditText;
	protected TextWidget m_IgnoreIDtext;
	protected ImageWidget m_CrosshairDot;
	protected ImageWidget m_dot_white;
	protected ImageWidget m_dot;
	protected ImageWidget m_dot0;
	protected Widget TextBackground;
	
	protected Widget m_IdentityPanel;
	protected Widget m_CrossHairs;
	protected Widget m_dzr_NotesGui;
	protected Widget m_PlayerIDicon;
	
	protected Widget m_dzrBaseMaskFrame;
	protected Widget m_dzrHalfMaskFrame;
	protected Widget m_dzrFullMaskFrame;
	protected Widget m_dzrIgnoreMaskFrame;
	protected Widget m_dzrIgnoreMaskIcon;
	protected Widget m_dzrIgnoreMaskIDIcon;
	protected Widget dzr_NotesGui;
	protected Widget NameCursorFrame;
	protected Widget CrossHairs;
	
	//TODO bool isInClassnames(string classname) не добавляет расстояние, а так вроде работает.
	private bool DZR_RecognitionToggle = false;
	bool m_LocalHold;
	//private bool m_NotShowing = false;
	private float g_TargetDistance;
	PlayerBase m_CurrentTargetPlayer;
	Object m_CursorObject;
	
	bool idz_key = false;
	bool holdingActive = false;
	bool idzActive = false;	
	bool stateUpdaterActive = false;
	
	bool g_amIinAnyMask = false;
	bool g_amIinFullMask = false;
	bool g_amIinHalfMask = false;
	bool g_IdzIconToggle = true;
	
	bool recheckPlayer = false;
	
	bool theNameHidden = true;
	
	string g_TargetName = " ";
	
	bool g_isTargetShowingName = false;
	bool g_amIShowingNameToAll = false;
	bool g_amIShowingID = false;
	
	
	bool m_maskIconShown;
	//string DZR_PreviousCharacterName = " ";
	
	ref dzr_idz_config_data_class g_ClientConfig;
	ref dzr_idz_config_data_class g_ClientConfig2;
	
	//float NEW_VisibleDistance_m;
	
	float g_MaskedDistance = 0;
	float g_SELF_MaskedDistance = 0;
	float g_SELF_IsInHalfMask = 0;
	float g_SELF_m_IsInMask = 0;
	
	float measuredDistance;
	
	//CONFIG
	int		RecognitionMode;
	int		ShowNameMode;
	int		MaskingMode;
	int		Crosshair;
	int		MaskingIndication;
	bool	AllowManualShow = false;
	bool	InstantManualShow = false;
	bool	ShowAliveName = false;
	bool	ShowDeadName = false;	
	//string	IdClassname;
	//string	RecognitionKeyInput;
	//string	ManualNameShowKeyInput;
	ref array<string> IdClassnames;
	ref array<string> FullMaskClassnames;
	int		FullMaskHide_m;
	ref array<string> HalfMaskClassnames;
	int		HalfMaskHide_m;
	ref array<string> HalfEyeMaskClassnames;
	int		HalfEyeMaskHide_m;
	ref array<string> GasMaskClassnames;
	int		GasMaskHide_m;
	ref array<string> ExcludedHeadgear;
	float	VisibleDistance_m;
	float	minVisibleDistance_m;
	float	DelayShow_ms;
	float	DelayHide_ms;
	float	NameUpdateFrequency_ms;
	float	DeadDistanceModifier_m;	
	int		NightMultiplier;
	int		NightModifier_m;
	int		OvercastModifier_m;
	float	FogModifier_m;
	bool	EnableDebug = false;
	string	Changelog;
	string	ModVersion;
	int		ConfigVersion;
	//CONFIG
	
	const protected static string m_configFile = "$profile:/DZR/IdentityZ/config.json";
	bool m_dzr_Debug = true;
	
	void MissionGameplay()
	{
		
		GetRPCManager().AddRPC( "DZR_IDZ_RPC", "DZR_MissionGameplay_GetNameFromServer", this, SingleplayerExecutionType.Both );
		GetRPCManager().AddRPC( "DZR_IDZ_RPC_TOclient", "DZR_MissionGameplay_SaveConfigOnClient", this, SingleplayerExecutionType.Both );
		GetRPCManager().AddRPC( "DZR_IDZ_RPC_TOclient", "DZR_MissionGameplay_SetShowingNameForSelf", this, SingleplayerExecutionType.Both );
		
		//GetRPCManager().SendRPC("DZR_IDZ_RPC", "DZR_MissionServer_GetConfig", null, true);
		
		//g_ClientConfig2 = GetDayZGame().ReadServerConfig();
		Print("[DZR IdentityZ][Client] ::: MissionGameplay ::: Initiated");
		
		g_ClientConfig = new dzr_idz_config_data_class();
		GetUApi().RegisterGroup( "core", "STR_USRACT_GROUP_EXPANSION");
		GetUApi().ListAvailableButtons();
		
		UAInput input = GetUApi().RegisterInput( "DZRCloseIdZ_LM", "Reset DZR Indentity 1", "core" );
		if(!input)
		{
			input.AddAlternative();
			input.BindCombo( "mBLeft" );
		}
		input = GetUApi().RegisterInput( "DZRCloseIdZ_RM", "Reset DZR Indentity 2", "core" );
		if(!input)
		{
			input.AddAlternative();
			input.BindCombo( "mBRight" );
		}
		input = GetUApi().RegisterInput( "DZRCloseIdZ_TAB", "Reset DZR Indentity 3", "core" );
		if(!input)
		{
			input.AddAlternative();
			input.BindCombo( "kTab" );
		}
		input = GetUApi().RegisterInput( "DZRCloseIdZ_ESC", "Reset DZR Indentity 4", "core" );
		if(!input)
		{
			input.AddAlternative();
			input.BindCombo( "kEscape" );
		}
		input = GetUApi().RegisterInput( "DZRCloseIdZ_F", "Reset DZR Indentity 5", "core" );
		if(!input)
		{
			input.AddAlternative();
			input.BindCombo( "kF" );
		}
		input = GetUApi().RegisterInput( "DZRCloseIdZ_CTRL", "Reset DZR Indentity 6", "core" );
		if(!input)
		{
			input.AddAlternative();
			input.BindCombo( "klControl" );
		}
		/*
			input = GetUApi().RegisterInput( "dzr_ToggleConfigModifier", "Toggle modifier showing Name to everyone", "core" );
			input.AddAlternative();
			input.BindCombo( "krControl" );
			
			input = GetUApi().RegisterInput( "dzr_IconToggle", "Toggle icon visibility", "core" );
			input.AddAlternative();
			input.BindCombo( "kGrave" );
		*/
		/*
			protected Widget m_IdentityPanel;
			protected Widget m_CrossHairs;
			protected Widget m_dzr_NotesGui;
			
			protected Widget m_dzrBaseMaskFrame;
			protected Widget m_dzrHalfMaskFrame;
			protected Widget m_dzrFullMaskFrame;
			protected Widget m_dzrIgnoreMaskFrame;
			protected Widget m_dzrIgnoreMaskIDIcon;
			protected Widget dzr_NotesGui;
			protected Widget NameCursorFrame;
		*/
		dzr_Print("Activating UI");
		m_DZRIDz_root = GetGame().GetWorkspace().CreateWidgets( "DZR_IDz/GUI/layouts/IdentityZ.layout" );
		
		
		dzr_Print("Activating UI: "+m_DZRIDz_root);
		m_IdentityPanel = Widget.Cast( m_DZRIDz_root.FindAnyWidget("IdentityPanel") );
		m_CrossHairs = Widget.Cast( m_DZRIDz_root.FindAnyWidget("CrossHairs") );
		m_dot_white = ImageWidget.Cast( m_DZRIDz_root.FindAnyWidget("dot_white") );
		m_dot = ImageWidget.Cast( m_DZRIDz_root.FindAnyWidget("dot") );
		m_dot0 = ImageWidget.Cast( m_DZRIDz_root.FindAnyWidget("dot0") );
		m_dzr_NotesGui = Widget.Cast( m_DZRIDz_root.FindAnyWidget("dzr_NotesGui") );
		m_dzrBaseMaskFrame = Widget.Cast( m_DZRIDz_root.FindAnyWidget("dzrBaseMaskFrame") );
		m_dzrHalfMaskFrame = Widget.Cast( m_DZRIDz_root.FindAnyWidget("dzrHalfMaskFrame") );
		m_dzrFullMaskFrame = Widget.Cast( m_DZRIDz_root.FindAnyWidget("dzrFullMaskFrame") );
		m_dzrIgnoreMaskFrame = Widget.Cast( m_DZRIDz_root.FindAnyWidget("dzrIgnoreMaskFrame") );
		m_dzrIgnoreMaskIcon = Widget.Cast( m_DZRIDz_root.FindAnyWidget("dzrIgnoreMaskIcon") );
		m_dzrIgnoreMaskIDIcon = Widget.Cast( m_DZRIDz_root.FindAnyWidget("dzrIgnoreMaskIDIcon") );
		m_IgnoreIDtext = TextWidget.Cast( m_DZRIDz_root.FindAnyWidget("IDtext") );
		
		NameCursorFrame = Widget.Cast( m_DZRIDz_root.FindAnyWidget("NameCursorFrame") );
		m_dzrCenterText = TextWidget.Cast( m_DZRIDz_root.FindAnyWidget("dzrCenterText") );
		
		dzr_NotesGui = Widget.Cast( m_DZRIDz_root.FindAnyWidget("dzr_NotesGui") );
		m_dzr_EditText = TextWidget.Cast( m_DZRIDz_root.FindAnyWidget("dzr_EditText") );
		
		m_DZRIDz_root.Show(true);
		m_IdentityPanel.Show(true);
		
		m_dzr_NotesGui.Show(false);
		m_dzrBaseMaskFrame.Show(false);
		m_dzrHalfMaskFrame.Show(false);
		m_dzrFullMaskFrame.Show(false);
		m_dzrIgnoreMaskFrame.Show(false);
		m_dzrIgnoreMaskIDIcon.Show(false);
		m_dzrIgnoreMaskIcon.Show(false);
		m_IgnoreIDtext.Show(false);
		
		NameCursorFrame.Show(true);
		m_dzrCenterText.Show(false);
		m_dzr_EditText.Show(false);
		
		m_CrossHairs.Show(true);
		m_dot.Show(false);
		m_dot_white.Show(false);
		m_dot0.Show(false);
		m_IgnoreIDtext.Show(false);
		
	}
	
	void showTargetName (PlayerBase target)
	{
		////GetGame().Chat("START showTargetName: "+target, "colorImportant");
		bool canShowName = false;
		string show_mode = "";
		string mask_mode = "NULL";
		
		if(target)
		{
			if(target.IsAlive())
			{
				DeadDistanceModifier_m = 0;
				if(ShowAliveName){
					show_mode = g_ClientConfig.ShowNameMode.ToString();
					mask_mode = g_ClientConfig.MaskingMode.ToString();
				}
				
				//dzr_Print("[DZR_IDz] ::: ALIVE ::: showTargetName DeadDistanceModifier_m: "+DeadDistanceModifier_m);
				
			}
			else 
			{
				//DeadDistanceModifier_m = g_ClientConfig.DeadDistanceModifier_m;
				if(isTargetShowingName())
				{
					DeadDistanceModifier_m = 0;
				} 
				else 
				{
					DeadDistanceModifier_m = g_ClientConfig.DeadDistanceModifier_m;
				}
				if(ShowDeadName){
					show_mode = g_ClientConfig.ShowNameMode.ToString();
					mask_mode = g_ClientConfig.MaskingMode.ToString();
				}
				
				dzr_Print("[DZR_IDz] ::: DEAD ::: showTargetName DeadDistanceModifier_m: "+DeadDistanceModifier_m);
			}
			string combined_mode = mask_mode+show_mode;
			if(combined_mode != "NULL")
			{
				
				switch (combined_mode){
					
					case "00":
					canShowName = true;
					break;
					
					case "01":
					if( isTargetShowingName() ){
						canShowName = true;
					}
					break;
					
					case "02":
					if( isTargetShowingID(target) )
					{
						canShowName = true;
					}
					break;
					
					case "10":
					if( !isTargetInMask(target) ){
						canShowName = true;
					}					
					break;
					
					case "11":				
					if( isTargetShowingName() ){
						canShowName = true;
					} 
					else 
					{
						if( !isTargetInMask(target) ){
							canShowName = true;
						}
					}
					break;
					
					case "12":				
					if( isTargetShowingID(target) ){
						canShowName = true;
					}
					else 
					{
						if( !isTargetInMask(target) ){
							canShowName = true;
						}
					}						
					break;
					
					case "20":
					calculateDistances(target);
					canShowName = true;
					break;
					
					case "21":
					calculateDistances(target);
					canShowName = true;
					break;
					
					case "22":
					if( isTargetShowingID(target) )
					{
						calculateDistances(target);
						canShowName = true;
					}
					break;
					
				}
			}
		}
		
		if(isTargetShowingName()){
			canShowName = true;
			////GetGame().Chat("Target allows to see name: "+canShowName, "colorImportant");
		}
		
		////GetGame().Chat("showTargetName canShowName: "+canShowName, "colorImportant");
		if(canShowName){
			if( isTargetInDistance(target) ){
				////GetGame().Chat("isTargetInDistance: true SO ShowNameWidget: "+canShowName, "colorImportant");
				GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(this.ShowNameWidget, DelayShow_ms, false, canShowName);
			}	
			else 
			{
				////GetGame().Chat("isTargetInDistance: false SO ShowNameWidget: false", "colorImportant");
				GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(this.ShowNameWidget, DelayHide_ms, false, false);
			}
		}
		else 
		{
			////GetGame().Chat("showTargetName canShowName: FALSE SO ShowNameWidget: "+canShowName, "colorImportant");
			GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(this.ShowNameWidget, DelayHide_ms, false, false);
		}
		
	}
	
	void ActivateIDZui(bool setting)
	{
		//dzr_Print("g_ClientConfig.Crosshair: "+g_ClientConfig.Crosshair);
		if(g_ClientConfig.Crosshair == 0){
			ShowCrosshairs(false);
			} else {
			ShowCrosshairs(setting);
			ShowGreenDot(setting);
			//NameCursorFrame.Show(setting);
		}
		
		if(!setting){
			m_dzrCenterText.SetText("");
		}
		
		
	}
	
	void ShowIgnoreMaskID(bool setting){
		//m_dzrBaseMaskFrame.Show(false);
		//m_dzrHalfMaskFrame.Show(false);
		//m_dzrFullMaskFrame.Show(false);
		//m_dzrIgnoreMaskFrame.Show(setting);
		//m_dzrIgnoreMaskIcon.Show(setting);
		m_dzrIgnoreMaskIDIcon.Show(setting);
		m_IgnoreIDtext.Show(setting);
	}
	
	void ShowIgnoreMask(bool setting){
		//m_dzrBaseMaskFrame.Show(false);
		//m_dzrHalfMaskFrame.Show(false);
		//m_dzrFullMaskFrame.Show(false);
		
		m_dzrIgnoreMaskFrame.Show(setting);
		m_dzrIgnoreMaskIcon.Show(setting);
		
		//m_dzrIgnoreMaskIDIcon.Show(false);
		//m_IgnoreIDtext.Show(false);
	}
	
	void ShowHalfMask(bool setting){
		m_dzrBaseMaskFrame.Show(setting);
		m_dzrHalfMaskFrame.Show(setting);
		//m_dzrFullMaskFrame.Show(false);
		//m_dzrIgnoreMaskFrame.Show(false);
		//m_dzrIgnoreMaskIDIcon.Show(false);
		//m_IgnoreIDtext.Show(false);
	}
	
	
	void ShowFullMask(bool setting){
		//m_dzrBaseMaskFrame.Show(false);
		//m_dzrHalfMaskFrame.Show(false);
		m_dzrFullMaskFrame.Show(setting);
		//m_dzrIgnoreMaskFrame.Show(false);
		//m_dzrIgnoreMaskIDIcon.Show(false);
		//m_IgnoreIDtext.Show(false);
	}
	
	void IdentityIconsToggle(bool setting){
		m_IdentityPanel.Show(setting);
	}
	
	void ShowIdentityPanel(bool setting){
		
		if(setting){
			setting = m_Hud.GetHudState();
		}
		
		if(g_IdzIconToggle)
		{
			
			m_IdentityPanel.Show(setting);
		}
	}
	
	void ShowAllIDZ(bool setting){
		
		ShowIdentityPanel(setting);
	}
	
	
	void ShowNameWidget(bool setting){		
		m_dzrCenterText.Show(setting);
	}
	
	void ShowCrosshairs(bool setting){
		m_CrossHairs.Show(setting);
		m_dot.Show(setting);
		m_dot_white.Show(setting);
		m_dot0.Show(setting);
		//dzr_Print("ShowCrosshairs: "+setting);
	}
	
	void ShowGreenDot(bool setting){
		m_dot.Show(setting);
		m_dot0.Show(setting);
		m_dot_white.Show(!setting);
	}
	
	void ShowWhiteDot(bool setting){
		m_dot.Show(setting);
		m_dot_white.Show(setting);
		m_dot0.Show(!setting);
	}
	
	
	void ~MissionGameplay()
	{
		dzr_Print("DZR_RecognitionToggle: "+DZR_RecognitionToggle);
		dzr_Print("Remove(this.RayCastForPlayer)");
		DZR_RecognitionToggle = false;
		GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).Remove(this.RayCastForPlayer);
		GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).Remove(this.updateSelfStates);
		
		m_DZRIDz_root = null;
		
		
	}
	
	//////////////////////////////// NEW LOGICS ////////////////////////////////
	
	bool isFullMask(string classname){
		if(FullMaskClassnames.Find(classname) != -1) {
			return true;
		}
		return false;
	};
	bool isHalfMask(string classname){
		if(HalfMaskClassnames.Find(classname) != -1) {
			return true;
		}
		return false;
	};
	bool isEyewearMask(string classname){
		if(HalfEyeMaskClassnames.Find(classname) != -1) {
			return true;
		}
		return false;
	};
	bool isGasMask(string classname){
		if(GasMaskClassnames.Find(classname) != -1) {
			return true;
		}
		return false;
	};
	bool isExcludedFromHeadgear(string classname){
		if(ExcludedHeadgear.Find(classname) != -1) {
			return true;
		}
		return false;
	};
	
	bool isTargetShowingID( PlayerBase targetPlayer, bool self = false ){
		if (self) 
		{
			targetPlayer = PlayerBase.Cast( GetGame().GetPlayer() );
			dzr_Print("isTargetShowingID: "+targetPlayer);
		}
		
		ItemBase itemInHand = ItemBase.Cast(targetPlayer.GetHumanInventory().GetEntityInHands());
		dzr_Print("itemInHand: "+itemInHand);
		if(itemInHand){		
			
			if(IdClassnames.Find( itemInHand.GetType() ) != -1) {
				return true;
			}
			
		}
		return false;
	}
	
	bool isTargetInDistance(PlayerBase targetPlayer)
	{
		float m_calc_nightMultiplier;
		float m_calc_NightModifier_m;
		float m_calc_overcastModifier;
		float m_calc_fogModifier;
		float m_calc_rainModifier;		
		PlayerBase player = PlayerBase.Cast( GetGame().GetPlayer() );
		if(targetPlayer) 
		{
			float NEW_VisibleDistance_m;
				
			
			if(!isTargetShowingName())
			{
				m_calc_nightMultiplier = (GetGame().GetWorld().IsNight())*NightMultiplier;
				m_calc_NightModifier_m = (GetGame().GetWorld().IsNight())*NightModifier_m;
				m_calc_overcastModifier = (GetGame().GetWeather().GetOvercast().GetActual())*OvercastModifier_m;
				m_calc_fogModifier = (GetGame().GetWeather().GetFog().GetActual())*FogModifier_m;
				m_calc_rainModifier = 0;
			};

			
			
			//
			if(EnableDebug)
			{
				measuredDistance = vector.Distance( GetGame().GetCurrentCameraPosition(), targetPlayer.GetPosition() );
			}
			else 
			{
			vector headPos = player.GetDamageZonePos("Head");
			measuredDistance = vector.Distance( headPos, targetPlayer.GetPosition() );
			}
			
			//g_MaskedDistance = FullMaskHide_m + HalfMaskHide_m + HalfEyeMaskHide_m + GasMaskHide_m;
			//////Print("[DZR IdentityZ]::: MissionGameplay ::g_MaskedDistance ("+g_MaskedDistance+") = FullMaskHide_m ("+FullMaskHide_m+") + HalfMaskHide_m ("+HalfMaskHide_m+") + HalfEyeMaskHide_m ("+HalfEyeMaskHide_m+") + GasMaskHide_m ("+HalfEyeMaskHide_m+");");
			
			
			
			//NEW_VisibleDistance_m = VisibleDistance_m - m_calc_NightModifier_m - m_calc_nightMultiplier*( m_calc_overcastModifier + m_calc_fogModifier) - g_MaskedDistance - DeadDistanceModifier_m;
			//float g_MaskedDistance = calcAllMasksModifier(thePlayer);
			
			NEW_VisibleDistance_m = VisibleDistance_m - m_calc_NightModifier_m - m_calc_nightMultiplier*( m_calc_overcastModifier + m_calc_fogModifier) - g_MaskedDistance - DeadDistanceModifier_m;
			
			if(NEW_VisibleDistance_m <= minVisibleDistance_m)
			{
				NEW_VisibleDistance_m = minVisibleDistance_m;
			}
			
			////GetGame().Chat("NEW_VisibleDistance_m [" +NEW_VisibleDistance_m+"] = " +VisibleDistance_m +" - "+ g_MaskedDistance, "colorImportant");
			////Print("NEW_VisibleDistance_m ["+NEW_VisibleDistance_m+"] = VisibleDistance_m ["+VisibleDistance_m+"] - m_calc_NightModifier_m ["+m_calc_NightModifier_m+"] - m_calc_nightMultiplier ["+m_calc_nightMultiplier+"] *( m_calc_overcastModifier ["+m_calc_overcastModifier+"] + m_calc_fogModifier ["+m_calc_fogModifier+"]) - g_MaskedDistance ["+g_MaskedDistance+"] - DeadDistanceModifier_m ["+DeadDistanceModifier_m+"]");
			
			if( (measuredDistance <= NEW_VisibleDistance_m) && (NEW_VisibleDistance_m >= 0.1) )
			{
				return true;
			}
			
		}
		
		return false;
	}
	
	void calculateDistances (PlayerBase thePlayer, bool self = false )
	{
		if (self) 
		{
			thePlayer = PlayerBase.Cast( GetGame().GetPlayer() );
			g_SELF_MaskedDistance = 0;
			} else {
			g_MaskedDistance = 0;
		}
		////Print("START calculateDistances for: "+thePlayer+", MaskingMode: "+MaskingMode);
		bool showFullMaskIcon = false;
		bool showHalfMaskIcon = false;
		
		
		if(thePlayer){
			
			
			
			ItemBase headgear = ItemBase.Cast(thePlayer.FindAttachmentBySlotName( "headgear" ));			
			string theItemName;
			if(headgear){
				// CHECK headgear
				theItemName = headgear.GetType();
				////Print("CHECK headgear: "+theItemName);
				if(theItemName){
					bool isExcludedFromHeadgear = isExcludedFromHeadgear( theItemName );
				}
				if(!isExcludedFromHeadgear)
				{					
					if( isFullMask( theItemName ) ){
						showFullMaskIcon = true;
						if (!self) { 
							
							g_MaskedDistance = g_MaskedDistance + g_ClientConfig.FullMaskHide_m; 
							////Print("Not SELF  g_MaskedDistance: "+g_MaskedDistance);
						}
						else
						{
							
							g_SELF_MaskedDistance = g_SELF_MaskedDistance + g_ClientConfig.FullMaskHide_m;
							
							////Print("calculateDistances self: "+self+"; g_SELF_MaskedDistance: "+g_SELF_MaskedDistance+"; showFullMaskIcon: "+showFullMaskIcon);
						}
						
					}
					if( isHalfMask( theItemName ) ){
						showHalfMaskIcon = true;
						if (!self) {g_MaskedDistance = g_MaskedDistance + g_ClientConfig.HalfMaskHide_m;}
						else
						{
							g_SELF_MaskedDistance = g_SELF_MaskedDistance + g_ClientConfig.HalfMaskHide_m;
						}
						
						//////Print("[DZR IdentityZ]::: MissionGameplay calcAllMasksModifier ::headgear: isHalfMask "+isHalfMask( theItemName )+ " mods g_MaskedDistance: "+g_MaskedDistance+", g_ClientConfig.HalfMaskHide_m:" +g_ClientConfig.HalfMaskHide_m);
					}
					if( isGasMask( theItemName ) ){
						showHalfMaskIcon = true;
						if (!self) {g_MaskedDistance = g_MaskedDistance + g_ClientConfig.GasMaskHide_m;}
						else
						{
							g_SELF_MaskedDistance = g_SELF_MaskedDistance + g_ClientConfig.GasMaskHide_m;
						}
						//////Print("[DZR IdentityZ]::: MissionGameplay calcAllMasksModifier ::headgear: isGasMask "+isGasMask( theItemName )+ " mods g_MaskedDistance: "+g_MaskedDistance+", g_ClientConfig.GasMaskHide_m:" +g_ClientConfig.GasMaskHide_m);
					}
					if( isEyewearMask( theItemName ) ){
						showHalfMaskIcon = true;
						if (!self) {g_MaskedDistance = g_MaskedDistance + g_ClientConfig.HalfEyeMaskHide_m;}
						else
						{
							g_SELF_MaskedDistance = g_SELF_MaskedDistance + g_ClientConfig.HalfEyeMaskHide_m;
						}
						//////Print("[DZR IdentityZ]::: MissionGameplay calcAllMasksModifier ::headgear: isEyewearMask "+isEyewearMask( theItemName )+ " mods g_MaskedDistance: "+g_MaskedDistance+", g_ClientConfig.HalfEyeMaskHide_m:" +g_ClientConfig.HalfEyeMaskHide_m);
					}
				}
				theItemName = "";
				headgear = NULL;
			}
			
			ItemBase mask = ItemBase.Cast(thePlayer.FindAttachmentBySlotName( "mask" ));
			if(mask){
				// CHECK mask
				theItemName = mask.GetType();
				////Print("CHECK mask: "+theItemName);
				if( isFullMask( theItemName ) ){
					showFullMaskIcon = true;
					if (!self) {g_MaskedDistance = g_MaskedDistance + g_ClientConfig.FullMaskHide_m;}
					else
					{
						g_SELF_MaskedDistance = g_SELF_MaskedDistance + g_ClientConfig.FullMaskHide_m;
						////Print("calculateDistances self: "+self+"; g_SELF_MaskedDistance: "+g_SELF_MaskedDistance+"; showFullMaskIcon: "+showFullMaskIcon);
					}
					//////Print("[DZR IdentityZ]::: MissionGameplay calcAllMasksModifier ::mask: isFullMask "+isFullMask( theItemName )+ " mods g_MaskedDistance: "+g_MaskedDistance+", g_ClientConfig.FullMaskHide_m:" +g_ClientConfig.FullMaskHide_m);
				}
				if( isHalfMask( theItemName ) ){
					showHalfMaskIcon = true;
					if (!self) {g_MaskedDistance = g_MaskedDistance + g_ClientConfig.HalfMaskHide_m;}
					else
					{
						g_SELF_MaskedDistance = g_SELF_MaskedDistance + g_ClientConfig.HalfMaskHide_m;
					}
					//////Print("[DZR IdentityZ]::: MissionGameplay calcAllMasksModifier ::mask: isHalfMask "+isHalfMask( theItemName )+ " mods g_MaskedDistance: "+g_MaskedDistance+", g_ClientConfig.HalfMaskHide_m:" +g_ClientConfig.HalfMaskHide_m);
				}
				if( isGasMask( theItemName ) ){
					showHalfMaskIcon = true;
					if (!self) {g_MaskedDistance = g_MaskedDistance + g_ClientConfig.GasMaskHide_m;}
					else
					{
						g_SELF_MaskedDistance = g_SELF_MaskedDistance + g_ClientConfig.GasMaskHide_m;
					}
					//////Print("[DZR IdentityZ]::: MissionGameplay calcAllMasksModifier ::mask: isGasMask "+isGasMask( theItemName )+ " mods g_MaskedDistance: "+g_MaskedDistance+", g_ClientConfig.GasMaskHide_m:" +g_ClientConfig.GasMaskHide_m);
				}
				if( isEyewearMask( theItemName ) ){
					showHalfMaskIcon = true;
					if (!self) {g_MaskedDistance = g_MaskedDistance + g_ClientConfig.HalfEyeMaskHide_m;}
					else
					{
						g_SELF_MaskedDistance = g_SELF_MaskedDistance + g_ClientConfig.HalfEyeMaskHide_m;
					}
					//////Print("[DZR IdentityZ]::: MissionGameplay calcAllMasksModifier ::mask: isEyewearMask "+isEyewearMask( theItemName )+ " mods g_MaskedDistance: "+g_MaskedDistance+", g_ClientConfig.HalfEyeMaskHide_m:" +g_ClientConfig.HalfEyeMaskHide_m);
				}
				theItemName = "";
				mask = NULL;
			}
			
			ItemBase eyewear = ItemBase.Cast(thePlayer.FindAttachmentBySlotName( "eyewear" ));
			if(eyewear){
				// CHECK eyewear
				theItemName = eyewear.GetType();
				////Print("CHECK eyewear: "+theItemName);
				if( isFullMask( theItemName ) ){
					showFullMaskIcon = true;
					if (!self) {g_MaskedDistance = g_MaskedDistance + g_ClientConfig.FullMaskHide_m;}
					else
					{
						g_SELF_MaskedDistance = g_SELF_MaskedDistance + g_ClientConfig.FullMaskHide_m;
						////Print("calculateDistances self: "+self+"; g_SELF_MaskedDistance: "+g_SELF_MaskedDistance+"; showFullMaskIcon: "+showFullMaskIcon);
					}
					//////Print("[DZR IdentityZ]::: MissionGameplay calcAllMasksModifier ::eyewear: isFullMask "+isFullMask( theItemName )+ " mods g_MaskedDistance: "+g_MaskedDistance+", g_ClientConfig.FullMaskHide_m:" +g_ClientConfig.FullMaskHide_m);
				}
				if( isHalfMask( theItemName ) ){
					showHalfMaskIcon = true;
					if (!self) {g_MaskedDistance = g_MaskedDistance + g_ClientConfig.HalfMaskHide_m;}
					else
					{
						g_SELF_MaskedDistance = g_SELF_MaskedDistance + g_ClientConfig.HalfMaskHide_m;
					}
					//("[DZR IdentityZ]::: MissionGameplay calcAllMasksModifier ::eyewear: isHalfMask "+isHalfMask( theItemName )+ " mods g_MaskedDistance: "+g_MaskedDistance+", g_ClientConfig.HalfMaskHide_m:" +g_ClientConfig.HalfMaskHide_m);
				}
				if( isGasMask( theItemName ) ){
					showHalfMaskIcon = true;
					if (!self) {g_MaskedDistance = g_MaskedDistance + g_ClientConfig.GasMaskHide_m;}
					else
					{
						g_SELF_MaskedDistance = g_SELF_MaskedDistance + g_ClientConfig.GasMaskHide_m;
					}
					//////Print("[DZR IdentityZ]::: MissionGameplay calcAllMasksModifier ::eyewear: isGasMask "+isGasMask( theItemName )+ " mods g_MaskedDistance: "+g_MaskedDistance+", g_ClientConfig.GasMaskHide_m:" +g_ClientConfig.GasMaskHide_m);
				} 
				if( isEyewearMask( theItemName ) ){
					
					showHalfMaskIcon = true;
					if (!self) {g_MaskedDistance = g_MaskedDistance + g_ClientConfig.HalfEyeMaskHide_m;}
					else
					{
						g_SELF_MaskedDistance = g_SELF_MaskedDistance + g_ClientConfig.HalfEyeMaskHide_m;
					}
					//GetGame().Chat(theItemName+"  isEyewearMask: "+showHalfMaskIcon+", g_SELF_MaskedDistance: "+g_SELF_MaskedDistance, "colorImportant");
					//////Print("[DZR IdentityZ]::: MissionGameplay calcAllMasksModifier ::eyewear: isEyewearMask "+isEyewearMask( theItemName )+ " mods g_MaskedDistance: "+g_MaskedDistance+", g_ClientConfig.HalfEyeMaskHide_m:" +g_ClientConfig.HalfEyeMaskHide_m);
				}
				theItemName = "";
				eyewear = NULL;
			}
			
		}
		
		
		if (!showHalfMaskIcon && !showFullMaskIcon)
		{
			if (!self) {
				g_MaskedDistance = 0;
			}
			else 
			{
				g_SELF_MaskedDistance = 0;
			}
		}
		
		if(isTargetShowingName()){
			if (!self) {g_MaskedDistance = 0;}
		}
		
		
		// PROPER ICON PROCESS
		if ( self ){
			
			if (showFullMaskIcon || showHalfMaskIcon )
			{
				g_amIinAnyMask = true;
			}
			
			//g_amIinHalfMask = showHalfMaskIcon;
			//g_amIinFullMask = showFullMaskIcon;
			
			if(!g_amIinFullMask){
				g_amIinHalfMask = showHalfMaskIcon;
				} else {
				g_amIinFullMask = showFullMaskIcon;
			}
			
			if(g_SELF_MaskedDistance >= VisibleDistance_m)
			{
				g_amIinFullMask = true;
				g_amIinHalfMask = false;
			}
			
			
			//GetGame().Chat("SELF calculateDistances :: g_SELF_MaskedDistance: "+g_SELF_MaskedDistance+", g_amIinHalfMask: "+showHalfMaskIcon+", g_amIinFullMask: "+showFullMaskIcon+", g_amIinAnyMask: "+g_amIinFullMask, "colorImportant");
		}
		// PROPER ICON PROCESS
		/*
			GetGame().Chat("showFullMaskIcon: "+showFullMaskIcon+", showHalfMaskIcon: "+showHalfMaskIcon+", g_SELF_MaskedDistance: "+g_SELF_MaskedDistance, "colorImportant");
			GetGame().Chat("g_amIinHalfMask: "+g_amIinHalfMask+", g_amIinFullMask: "+g_amIinFullMask+", VisibleDistance_m: "+VisibleDistance_m, "colorImportant");
			GetGame().Chat("g_MaskedDistance: "+g_MaskedDistance, "colorImportant");
			
			//Print("showFullMaskIcon: "+showFullMaskIcon+", showHalfMaskIcon: +"+showHalfMaskIcon+", g_SELF_MaskedDistance: "+g_SELF_MaskedDistance);
			//Print("g_amIinHalfMask: "+g_amIinHalfMask+", g_amIinFullMask: +"+g_amIinFullMask+", VisibleDistance_m: "+VisibleDistance_m);
			//Print("g_MaskedDistance: "+g_MaskedDistance);
		*/
		////Print("STOP calculateDistances :: g_MaskedDistance: "+g_MaskedDistance+", showHalfMaskIcon: "+showHalfMaskIcon+", showFullMaskIcon: "+showFullMaskIcon );
	}
	
	bool isTargetInMask( PlayerBase thePlayer , bool self = false ){
		if (self) 
		{
			thePlayer = PlayerBase.Cast( GetGame().GetPlayer() );
		}
		///////GetGame().Chat("[IDZ] dzr_isWithinDistance faceVisible START g_isTargetShowingName: "+g_isTargetShowingName, "colorImportant");
		bool m_IsInMask = false;
		bool m_IsInHalfMask = false;
		
		if(thePlayer){
			
			ItemBase headgear = ItemBase.Cast(thePlayer.FindAttachmentBySlotName( "headgear" ));
			ItemBase mask = ItemBase.Cast(thePlayer.FindAttachmentBySlotName( "mask" ));
			ItemBase eyewear = ItemBase.Cast(thePlayer.FindAttachmentBySlotName( "eyewear" ));
			string theItemName;
			
			//////Print("[DZR IdentityZ]::: MissionGameplay faceVisible ::MaskingMode: "+MaskingMode);
			
			
			if(headgear){
				// CHECK headgear
				theItemName = headgear.GetType();
				if(!isExcludedFromHeadgear( theItemName ))
				{
					if( isFullMask( theItemName ) ){m_IsInMask = true; };
					if( isHalfMask( theItemName ) ){m_IsInMask = true; m_IsInHalfMask = true};
					if( isGasMask( theItemName ) ){m_IsInMask = true; m_IsInHalfMask = true};
					if( isEyewearMask( theItemName ) ){ m_IsInHalfMask = true};
					//////Print("[DZR IdentityZ]::: MissionGameplay !MaskingMode ::g_MaskedDistance: "+g_MaskedDistance);
				}
			}
			
			if(mask){
				// CHECK mask
				theItemName = mask.GetType();
				if( isFullMask( theItemName ) ){m_IsInMask = true; };
				if( isHalfMask( theItemName ) ){m_IsInMask = true; m_IsInHalfMask = true};
				if( isGasMask( theItemName ) ){m_IsInMask = true; m_IsInHalfMask = true};
				if( isEyewearMask( theItemName ) ){ m_IsInHalfMask = true};
				
			}
			if(eyewear){
				// CHECK mask
				theItemName = eyewear.GetType();
				if( isFullMask( theItemName ) ){m_IsInMask = true; };
				if( isHalfMask( theItemName ) ){m_IsInMask = true; m_IsInHalfMask = true};
				if( isGasMask( theItemName ) ){m_IsInMask = true; m_IsInHalfMask = true};
				if( isEyewearMask( theItemName ) ){ m_IsInHalfMask = true};
			}
			
		} 
		else 
		{
			m_IsInMask = false;
		}
		
		// PROPER ICON PROCESS
		if ( self ){
			
			if(MaskingMode == 1) {
				g_amIinAnyMask = m_IsInMask;
				g_amIinFullMask = m_IsInMask;
				g_amIinHalfMask = false;
				////Print("isTargetInMask SELF g_amIinAnyMask: "+m_IsInMask +", g_amIinFullMask: "+m_IsInMask+", g_amIinHalfMask: "+false);
			}
			
			if(MaskingMode == 2) {
				g_amIinAnyMask = false;
				g_amIinFullMask = m_IsInMask;
				g_amIinHalfMask = m_IsInHalfMask;
				////Print("isTargetInMask SELF g_amIinAnyMask: "+m_IsInMask +", g_amIinFullMask: "+m_IsInMask+", g_amIinHalfMask: "+false);
			}
		}
		// PROPER ICON PROCESS
		
		////Print("g_MaskedDistance: "+g_MaskedDistance +", m_IsInMask: "+m_IsInMask+", m_IsInHalfMask: "+m_IsInHalfMask);
		return m_IsInMask;
	}
	
	bool isTargetShowingName()
	{
		if(g_ClientConfig){
			if(AllowManualShow)
			{
				if(g_isTargetShowingName){
					
					//DeadDistanceModifier_m = 0;
					NightMultiplier = 0;
					NightModifier_m = 0;
					OvercastModifier_m = 0;
					FogModifier_m = 0;
					
					//dzr_Print("[DZR_IDz] ::: ALIVE ::: +g_isTargetShowingName() DeadDistanceModifier_m: "+DeadDistanceModifier_m);
					
					if(InstantManualShow){
						DelayShow_ms = 0;
						DelayHide_ms = 0;			
					} 
					else 
					{
						DelayShow_ms = g_ClientConfig.DelayShow_ms;
						DelayHide_ms = g_ClientConfig.DelayHide_ms;
						
					}
					return true;
				} 
				else
				{
					//DeadDistanceModifier_m = g_ClientConfig.DeadDistanceModifier_m;
					NightMultiplier = g_ClientConfig.NightMultiplier;
					NightModifier_m = g_ClientConfig.NightModifier_m;
					OvercastModifier_m = g_ClientConfig.OvercastModifier_m;
					FogModifier_m = g_ClientConfig.FogModifier_m;	
					//dzr_Print("[DZR_IDz] ::: ALIVE ::: !g_isTargetShowingName() DeadDistanceModifier_m: "+DeadDistanceModifier_m);
				}
			}
		}
		return false;
	}
	
	
	
	
	
	
	void DZR_MissionGameplay_SetShowingNameForSelf(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender, ref Object target)
	{
		
		Param1<bool> data;
		if ( !ctx.Read( data ) ) return;
		
		if (type == CallType.Client)
		{	
			g_amIShowingNameToAll = data.param1;
			
			
			///////GetGame().Chat("||| Got g_amIShowingNameToAll RPC from server: "+g_amIShowingNameToAll, "colorImportant");
			////GetGame().Chat("||| g_isTargetShowingName: "+g_isTargetShowingName, "colorImportant");
			
		}
	}
	
	void DZR_MissionGameplay_SaveConfigOnClient(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender, ref Object target)
	{
		
		Param1<dzr_idz_config_data_class> data;
		if ( !ctx.Read( data ) ) return;
		
		if (type == CallType.Client)
		{
			g_ClientConfig = data.param1;
			////Print("[DZR IdentityZ]::: DZR_MissionGameplay_SaveConfigOnClient :: Reading game to store on client ");
			if(g_ClientConfig){
				
				
				//CONFIG
				RecognitionMode = g_ClientConfig.RecognitionMode;
				ShowNameMode = g_ClientConfig.ShowNameMode;
				MaskingMode = g_ClientConfig.MaskingMode;
				Crosshair = g_ClientConfig.Crosshair;
				MaskingIndication = g_ClientConfig.MaskingIndication;
				AllowManualShow = g_ClientConfig.AllowManualShow;
				InstantManualShow = g_ClientConfig.InstantManualShow;
				ShowAliveName = g_ClientConfig.ShowAliveName;
				ShowDeadName = g_ClientConfig.ShowDeadName;
				//IdClassname = g_ClientConfig.IdClassname;
				IdClassnames = g_ClientConfig.IdClassnames;
				//RecognitionKeyInput = g_ClientConfig.RecognitionKeyInput;
				//ManualNameShowKeyInput = g_ClientConfig.ManualNameShowKeyInput;
				FullMaskClassnames = g_ClientConfig.FullMaskClassnames;
				FullMaskHide_m = g_ClientConfig.FullMaskHide_m;
				HalfMaskClassnames = g_ClientConfig.HalfMaskClassnames;
				HalfMaskHide_m = g_ClientConfig.HalfMaskHide_m;
				HalfEyeMaskClassnames = g_ClientConfig.HalfEyeMaskClassnames;
				HalfEyeMaskHide_m = g_ClientConfig.HalfEyeMaskHide_m;
				GasMaskClassnames = g_ClientConfig.GasMaskClassnames;
				ExcludedHeadgear = g_ClientConfig.ExcludedHeadgear;
				GasMaskHide_m = g_ClientConfig.GasMaskHide_m;
				VisibleDistance_m = g_ClientConfig.VisibleDistance_m;
				minVisibleDistance_m = g_ClientConfig.minVisibleDistance_m;
				DelayShow_ms = g_ClientConfig.DelayShow_ms;
				DelayHide_ms = g_ClientConfig.DelayHide_ms;
				NameUpdateFrequency_ms = g_ClientConfig.NameUpdateFrequency_ms;
				DeadDistanceModifier_m = g_ClientConfig.DeadDistanceModifier_m;
				NightMultiplier = g_ClientConfig.NightMultiplier;
				NightModifier_m = g_ClientConfig.NightModifier_m;
				OvercastModifier_m = g_ClientConfig.OvercastModifier_m;
				FogModifier_m = g_ClientConfig.FogModifier_m;
				EnableDebug = g_ClientConfig.EnableDebug;
				Changelog = g_ClientConfig.Changelog;
				ConfigVersion = g_ClientConfig.ConfigVersion;
				ModVersion = g_ClientConfig.ModVersion;
				//CONFIG
				
				m_dzr_Debug = g_ClientConfig.EnableDebug;
				
				Print("[DZR IdentityZ] ::: ACTIVE (Version: "+g_ClientConfig.ModVersion+"."+g_ClientConfig.ConfigVersion+" DEBUG: "+g_ClientConfig.EnableDebug+")");
				Print("[DZR IdentityZ] ::: RecognitionMode: "+g_ClientConfig.RecognitionMode );
				Print("[DZR IdentityZ] ::: ShowNameMode: "+g_ClientConfig.ShowNameMode );
				Print("[DZR IdentityZ] ::: MaskingMode: "+g_ClientConfig.MaskingMode );
				Print("[DZR IdentityZ] ::: Crosshair: "+g_ClientConfig.Crosshair );
				Print("[DZR IdentityZ] ::: AllowManualShow: "+g_ClientConfig.AllowManualShow );
				Print("[DZR IdentityZ] ::: InstantManualShow: "+g_ClientConfig.InstantManualShow );
				Print("[DZR IdentityZ] ::: ShowAliveName: "+g_ClientConfig.ShowAliveName );
				Print("[DZR IdentityZ] ::: ShowDeadName: "+g_ClientConfig.ShowDeadName );
				
				////Print("[DZR IdentityZ]::: DZR_MissionGameplay_SaveConfigOnClient :: Reading EnableDebug " + g_ClientConfig.EnableDebug);
				//Print("[DZR IdentityZ] ::: RecognitionMode: "+RecognitionMode);
				//Print("[DZR IdentityZ] ::: ShowNameMode: "+ShowNameMode);
				//Print("[DZR IdentityZ] ::: MaskingMode: "+MaskingMode);
				//Print("[DZR IdentityZ] ::: Crosshair: "+Crosshair);
				//Print("[DZR IdentityZ] ::: MaskingIndication: "+MaskingIndication);
				//Print("[DZR IdentityZ] ::: AllowManualShow: "+AllowManualShow);
				//Print("[DZR IdentityZ] ::: InstantManualShow: "+InstantManualShow);
				//Print("[DZR IdentityZ] ::: ShowAliveName: "+ShowAliveName);
				//Print("[DZR IdentityZ] ::: ShowDeadName: "+ShowDeadName);
				/*
					GetUApi().ListAvailableButtons();
					UAInput input = GetUApi().RegisterInput( "dzr_ToggleConfig", "Toggle showing Name to everyone", "core" );
					input.AddAlternative();
					input.BindCombo( ManualNameShowKeyInput );
					input = GetUApi().RegisterInput( "UAUIDZRIDzToggle", "#STR_IDZ_ACTIVATE_RECOGNITION", "core" );
					input.AddAlternative();
					input.BindCombo( RecognitionKeyInput );
				*/
				////Print("[DZR IdentityZ] ::: RecognitionKeyInput: "+RecognitionKeyInput);
				////Print("[DZR IdentityZ] ::: ManualNameShowKeyInput: "+ManualNameShowKeyInput);
				
			}
		}
	}
	
	
	
	void dzr_Print(string strDebugMessage)
	{
		if(g_ClientConfig.EnableDebug){
			GetGame().Chat("[DZR IdentityZ]::: MissionGameplay :: "+strDebugMessage, "colorImportant");
			Print("[DZR IdentityZ]::: MissionGameplay :: "+strDebugMessage);
		}
	};
	
	//////////////////////////////// NEW LOGICS ////////////////////////////////	
	
	
	void RayCastForPlayer()
	{
		PlayerBase player = PlayerBase.Cast( GetGame().GetPlayer() );
		
		vector rayStart = GetGame().GetCurrentCameraPosition() + GetGame().GetCurrentCameraDirection() * 0.47;
		vector rayEnd = GetGame().GetCurrentCameraPosition() + GetGame().GetCurrentCameraDirection() * (VisibleDistance_m + 1);		
		
		float distance;
		vector position;
		vector contact_dir;
		int contact_component;
		ref set<Object> hitObjects = new set<Object>;
		
		if(EnableDebug)
		{
			DayZPhysics.RaycastRV(rayStart, rayEnd, position, contact_dir, contact_component, hitObjects, null, null, false, false, ObjIntersectFire, 0.1);
		}
		else 
		{
			DayZPhysics.RaycastRV(rayStart, rayEnd, position, contact_dir, contact_component, hitObjects, null, player, false, false, ObjIntersectFire, 0.1);
		}
		
		if ( hitObjects.Count() > 0 )
		{
			m_CursorObject = hitObjects.Get(0);
			
		} 
		else 
		{
			m_CursorObject = null;
		}
		
		
		
		if( GetGame().ObjectIsKindOf(m_CursorObject,"SurvivorBase") )
		{
			
			m_CurrentTargetPlayer = PlayerBase.Cast(m_CursorObject);		
			
			ref Param1<PlayerBase> param = new Param1<PlayerBase>(m_CurrentTargetPlayer);
			GetRPCManager().SendRPC( "DZR_IDZ_RPC", "DZR_MissionServer_GetDataFromServer", param, true);
			showTargetName(m_CurrentTargetPlayer);	
			dzr_Print("m_CurrentTargetPlayer: "+m_CurrentTargetPlayer);
		} 
		else 
		{
			dzr_Print("m_CursorObject: "+m_CursorObject);
			if(!EnableDebug){
				m_CursorObject = null;
			}
			m_CurrentTargetPlayer = null;
			showTargetName(m_CurrentTargetPlayer);
		}
	}
	
	
	void DZR_MissionGameplay_GetNameFromServer( CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender, ref Object target )
	{
		
		Param2<string, bool> data;
		if ( !ctx.Read( data ) ) return;
		
		if (type == CallType.Client)
		{
			
			g_TargetName = data.param1;
			g_isTargetShowingName = data.param2;
			if(EnableDebug)
			{
				m_dzrCenterText.SetText(g_TargetName+" [Cur: "+measuredDistance+"m Max: "+VisibleDistance_m+"m - Mask: "+g_MaskedDistance+"m - Dead: "+DeadDistanceModifier_m+"m] Manual: "+g_isTargetShowingName);
			}
			else 
			{
				m_dzrCenterText.SetText(g_TargetName);
			}
			////GetGame().Chat("Got RPC from server: g_isTargetShowingName: "+g_isTargetShowingName+", g_TargetName:"+g_TargetName,"colorImportant");
			//////Print("g_TargetName: "+g_TargetName+" g_isTargetShowingName: "+g_isTargetShowingName);
			
			
		}
	}	
	
	
	void updateSelfStates()
	{
		PlayerBase player = PlayerBase.Cast( GetGame().GetPlayer() );
		if(player)
		{
			
			bool IgnoreManual = false;
			bool IgnoreWithID = false;
			bool FullMaskShow = false;
			bool HalfMaskShow = false;
			
			////Print("Updating my Statuses g_amIShowingNameToAll: "+g_amIShowingNameToAll);
			// IDZ UI SELF
			bool hasID;
			
			// PROPER ICON UPDATE
			if(MaskingMode ==0){
				
				
				if(ShowNameMode == 0)
				{
					IgnoreWithID = false;
					IgnoreManual = false;
					FullMaskShow = false;
					HalfMaskShow = false;
				}
				
				if(ShowNameMode == 1)
				{
					if(g_amIShowingNameToAll) 
					{
						IgnoreManual = true;
						IgnoreWithID = false;
						FullMaskShow = false;
						HalfMaskShow = false;
					};				
				}
				
				if(ShowNameMode == 2)
				{
					hasID = isTargetShowingID(player, true);
					if(hasID) 
					{
						IgnoreManual = false;
						IgnoreWithID = true;
						FullMaskShow = false;
						HalfMaskShow = false;
					};
					
				}
				
				
			}
			
			if(MaskingMode ==1){
				
				
				if(ShowNameMode == 0)
				{
					IgnoreManual = false;
					IgnoreWithID = false;
					FullMaskShow = false;
					HalfMaskShow = false;
				}
				
				if(ShowNameMode == 1)
				{
					if(g_amIShowingNameToAll) 
					{
						IgnoreManual = true;
						IgnoreWithID = false;
						FullMaskShow = false;
						HalfMaskShow = false;
					}
					else 
					{
						isTargetInMask(player);
						if(g_amIinAnyMask){
							IgnoreManual = false;
							IgnoreWithID = false;
							FullMaskShow = true;
							HalfMaskShow = false;
						}
					}					
				}
				
				if(ShowNameMode == 2)
				{
					hasID = isTargetShowingID(player, true);
					if(hasID) 
					{
						IgnoreManual = false;
						IgnoreWithID = true;
					}
					else 
					{
						isTargetInMask(player);
						if(g_amIinAnyMask){
							IgnoreManual = false;
							IgnoreWithID = false;
							FullMaskShow = true;
							HalfMaskShow = false;
						}
					}
					
				}
				
			}
			
			if(MaskingMode ==2)
			{
				////Print("MaskingMode: "+MaskingMode);
				
				if(ShowNameMode == 0)
				{
					IgnoreManual = false;
					IgnoreWithID = false;
					FullMaskShow = false;
					HalfMaskShow = false;
				}
				
				if(ShowNameMode == 1)
				{
					////Print("ShowNameMode: "+ShowNameMode);
					if(g_amIShowingNameToAll) 
					{
						////Print("ShowNameMode: "+ShowNameMode);
						IgnoreManual = true;
						IgnoreWithID = false;
						FullMaskShow = false;
						HalfMaskShow = false;
					}
					else 
					{
						calculateDistances(player, true);
						////Print("not showing Name");
						if(g_amIinFullMask){
							////Print("g_amIinFullMask: "+g_amIinFullMask);
							IgnoreManual = false;
							IgnoreWithID = false;
							FullMaskShow = true;
							HalfMaskShow = false;
						}
						else 
						{
							if(g_amIinHalfMask){
								////Print("g_amIinHalfMask: "+g_amIinHalfMask);
								IgnoreManual = false;
								IgnoreWithID = false;
								FullMaskShow = false;
								HalfMaskShow = true;
							}
						}
					}					
				}
				
				if(ShowNameMode == 2)
				{
					hasID = isTargetShowingID(player, true);
					if(hasID) 
					{
						IgnoreManual = false;
						IgnoreWithID = true;
					}
					else 
					{
						calculateDistances(player, true);
						//isTargetInMask(player);
						if(g_amIinFullMask){
							IgnoreManual = false;
							IgnoreWithID = false;
							FullMaskShow = true;
							HalfMaskShow = false;
						}
						else 
						{
							if(g_amIinHalfMask){
								IgnoreManual = false;
								IgnoreWithID = false;
								FullMaskShow = false;
								HalfMaskShow = true;
							}
						}
					}
					
				}
				
			}
			if(IgnoreWithID){
				ShowIgnoreMaskID(IgnoreWithID);
				ShowIgnoreMask(IgnoreWithID);
				} else {
				ShowIgnoreMask(IgnoreManual);
			}
			ShowFullMask(FullMaskShow);
			ShowHalfMask(HalfMaskShow);
			dzr_Print("IgnoreWithID: "+IgnoreWithID);
			// PROPER ICON UPDATE
			////Print("[DZR IdentityZ] m_Hud.GetHudState()	: "+!m_Hud.GetHudState());
			m_DZRIDz_root.Show(m_Hud.GetHudState());
			//IdentityIconsToggle(m_Hud.GetHudState());
			//ActivateIDZui(m_Hud.GetHudState());
			// IDZ UI SELF
			//stateUpdaterActive = false;
		}
	}
	
	override void OnUpdate(float timeslice)
	{
		super.OnUpdate( timeslice );
		PlayerBase player = PlayerBase.Cast( GetGame().GetPlayer() );
		
		
		
		
		
		if(player) {
			
			// IDZ UI SELF
			if(MaskingIndication != 0)
			{
				ShowIdentityPanel(true);
				if(!stateUpdaterActive){
					GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(this.updateSelfStates, NameUpdateFrequency_ms, true);
					stateUpdaterActive = true;
				}	
			}
			
			
			
			// IDZ UI SELF
			
			
			PlayerIdentity identity = player.GetIdentity();
			/*
				if ( GetUApi().GetInputByName( "dzr_ToggleConfigModifier" ).LocalHold() )
				{
				
				m_LocalHold = true;
				}else{
				m_LocalHold = false;
				}
			*/
			//if ( GetUApi().GetInputByName( "UAUIDZRIDzNameShow" ).LocalRelease() && m_LocalHold )
			if ( GetUApi().GetInputByName( "UAUIDZRIDzNameShow" ).LocalRelease() )
			{
				
				if(AllowManualShow){
					//	GetGame().Chat("UAUIDZRIDzNameShow: "+UAUIDZRIDzNameShow, "colorImportant");
					
					ref Param2<PlayerBase, int> param = new Param2< PlayerBase, int>(player, 1);
					GetRPCManager().SendRPC( "DZR_IDZ_RPC", "DZR_MissionServer_ToggleShowToAll", param, true);
				};
			}
			//CTRL+P Show name to all
			
			if ( GetUApi().GetInputByName( "DZRCloseIdZ_RM" ).LocalHold() || GetUApi().GetInputByName( "DZRCloseIdZ_LM" ).LocalHold() || GetUApi().GetInputByName( "DZRCloseIdZ_LM" ).LocalPress() || GetUApi().GetInputByName( "DZRCloseIdZ_TAB" ).LocalPress() || GetUApi().GetInputByName( "DZRCloseIdZ_ESC" ).LocalPress() || GetUApi().GetInputByName( "DZRCloseIdZ_F" ).LocalPress() || !player.IsAlive() || player.GetInputController().IsWeaponRaised())
			{
				if(RecognitionMode != 0)
				{
					DZR_RecognitionToggle = false;
					GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).Remove(this.RayCastForPlayer);
				}
				if(ShowNameMode == 0)
				{
					ActivateIDZui(false);
					
				}
				
				if(MaskingIndication == 2){
					ShowIdentityPanel(false);
				}
				
				
			};
			
			if(RecognitionMode == 0){
				
				ActivateIDZui(true);
				
				if(!idzActive){
					GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(this.RayCastForPlayer, NameUpdateFrequency_ms, true);
					idzActive = true;
				}		
			}
			
			if(RecognitionMode == 1){ //toggle
				
				if(GetUApi().GetInputByName( "UAUIDZRIDzToggle" ).LocalRelease())
				{
					
					DZR_RecognitionToggle = !DZR_RecognitionToggle;
					dzr_Print("DZR_RecognitionToggle: "+DZR_RecognitionToggle);
					if (DZR_RecognitionToggle){
						if (!player.GetInputController().IsWeaponRaised()){
							
							//SHOW WIDGETS
							ActivateIDZui(true);
							if(MaskingIndication == 2){
								ShowIdentityPanel(true);
							}
							//SHOW WIDGETS
							
							GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(this.RayCastForPlayer, NameUpdateFrequency_ms, true);
						}
					} 
					else 
					{
						//player.GetInputController().OverrideRaise(false, false);
						
						
						//TextBackground.Show(false);
						GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).Remove(this.RayCastForPlayer);
						//SHOW WIDGETS
						ActivateIDZui(false);
						if(MaskingIndication == 2){
							ShowIdentityPanel(false);
						}
						//SHOW WIDGETS
					};
					
				}
			}
			
			if(RecognitionMode == 2){ //hold
				
				if(GetUApi().GetInputByName( "UAUIDZRIDzToggle" ).LocalHold())
				{
					if(!holdingActive){
						GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).CallLater(this.RayCastForPlayer, NameUpdateFrequency_ms, true);
						holdingActive = true;
					}
					
					ActivateIDZui(true);
					if(MaskingIndication == 2){
						ShowIdentityPanel(true);
					}
				} else 
				{
					GetGame().GetCallQueue(CALL_CATEGORY_GAMEPLAY).Remove(this.RayCastForPlayer);
					ActivateIDZui(false);
					holdingActive = false;
					ShowNameWidget(false);
					if(MaskingIndication == 2){
						ShowIdentityPanel(false);
					}					
				}
			}
			
		}
	}	
}		

